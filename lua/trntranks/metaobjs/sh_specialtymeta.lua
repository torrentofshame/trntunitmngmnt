--[[
		Specialty
]]
TrntUnits.Specialties = TrntUnits.Specialties or {}
Specialty = {}
Specialty.__index = Specialty

function Specialty:new(name, prefix, uid, models, weapons, teams)
    local NewSpecialty = {
        name = name,
        prefix = prefix,
        uid = uid,
        models = models or {},
        weapons = weapons or {},
        teams = teams or {},
        bonusstats = {},
        rankIconOverrides = {}
    }

    if type(NewSpecialty.models) ~= "table" then
        NewSpecialty.models = {models}
    end

    setmetatable(NewSpecialty, Specialty)
    TrntUnits.Specialties[uid] = NewSpecialty

    return NewSpecialty
end

function Specialty:GetWeps()
    return self.weapons
end

function Specialty:GetUID()
    return self.uid
end

function Specialty:addRankIconOverride(ranks, icon)
    if ranks then
        self.rankIconOverrides[ranks] = icon
    elseif ranks and type(ranks) == "table" then
        for _, v in pairs(ranks) do
            self.rankIconOverrides[v] = icon
        end
    end
end

function Specialty:SetDescription(...)
    local args = {...}
    self.description = ""

    for k, v in pairs(args) do
        if k == 1 then
            self.description = v
        else
            self.description = self.description .. "\n" .. v
        end
    end
end

function Specialty:Description()
    return self.description
end

function Specialty:getRankIconOverride(rank)
    return self.rankIconOverrides[rank]
end

function Specialty:CalcBonusHP( maxhp )
    if not maxhp then return end
    return maxhp + ( self.bonusstats.health or 0 )
end

function Specialty:CalcBonusSpeed( walkspeed, runspeed )
    if not walkspeed and not runspeed then return end
    return ( walkspeed + ( self.bonusstats.walkspeed or 0 ) ), ( runspeed + ( self.bonusstats.runspeed or 0 ) )
end

function Specialty:GetArmorBonus()
    return self.bonusstats.armor or 0
end

function Specialty:SetBonusStats( health, walkspeed, runspeed, armor )
    self.bonusstats.health = health or 0
    self.bonusstats.walkspeed = walkspeed or 0
    self.bonusstats.runspeed = runspeed or 0
    self.bonusstats.armor = armor or 0
end

setmetatable(Specialty, {
    __call = Specialty.new
})
