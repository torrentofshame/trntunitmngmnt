--[[
		RankSystem
]]
TrntUnits.RankSystems = TrntUnits.RankSystems or {}
RankSystem = {}
RankSystem.__index = RankSystem

function RankSystem:new(name, bonuspay, teams)
    local newRankSystem = {
        name = name,
        bonuspay = bonuspay or 0,
        teams = teams or {},
        ranks = {}
    }

    setmetatable(newRankSystem, RankSystem)
    TrntUnits.RankSystems[#TrntUnits.RankSystems + 1] = newRankSystem

    return newRankSystem
end

function RankSystem:setBonuspay(bonuspay)
    if bonuspay then
        self.bonuspay = bonuspay
    end
end

function RankSystem:addRank(rank)
    if rank then
        self.ranks[#self.ranks + 1] = rank
    end
end

function RankSystem:addTeam(team)
    if team then
        self.teams[#self.teams + 1] = team
    end
end

setmetatable(RankSystem, {
    __call = RankSystem.new
})

--[[
		Rank
]]
Rank = {}
Rank.__index = Rank

function Rank:new(RankSys, index, name, prefix, rankclass, perms, bonuswep, icon, customspawnfunc)
    local NewRank = {
        index = index,
        name = name,
        prefix = prefix or "",
        rankclass = rankclass,
        perms = perms or {},
        bonuswep = bonuswep or {},
        icon = icon,
        customspawnfunc = customspawnfunc
    }

    NewRank["RankSys"] = RankSys
    setmetatable(NewRank, Rank)
    RankSys.ranks[index] = NewRank

    return NewRank
end

function Rank:GetWeps()
    return self.bonuswep
end

function Rank:canPromote(rank)
    if self.perms.Promote and self.perms.MaxPromotion > rank.index and self.RankSys == rank.RankSys then return true end

    return false
end

function Rank:canDemote(rank)
    if self.perms.Demote and self.perms.MaxDemotion < rank.index and self.RankSys == rank.RankSys then return true end

    return false
end

function Rank:OffsetRank(offset)
    return self.RankSys.ranks[self.index + offset] or nil
end

function Rank:findBonusSalaryChange(basesalary)
    return math.floor(basesalary * ((self.RankSys.bonuspay / 100) * (self.index - 1)))
end

setmetatable(Rank, {
    __call = Rank.new
})
