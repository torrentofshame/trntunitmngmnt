--[[
		UnitTypes
]]
TrntUnits.UnitTypes = TrntUnits.UnitTypes or {}
TrntUnits.Units = TrntUnits.Units or {}
TrntUnits.TLUnits = TrntUnits.TLUnits or {}
UnitType = {}
UnitType.__index = UnitType

function UnitType:create(name, directsubunit, quantity)
    local NewUnitType = {
        name = name,
        directsubunit = directsubunit,
        quantity = quantity or 1
    }

    setmetatable(NewUnitType, UnitType)
    TrntUnits.UnitTypes[#TrntUnits.UnitTypes + 1] = NewUnitType

    return NewUnitType
end

function UnitType:new(parent, name, prefix, leadteam, defaultteam, teams)
    local NewUnit = {
        parent = parent,
        name = name,
        prefix = prefix or "",
        teams = teams or {},
        directsubunits = {},
        unittype = self
    }

    NewUnit.teams.Lead = leadteam
    NewUnit.teams.Default = defaultteam
    setmetatable(NewUnit, self)

    if parent ~= nil then
        parent.directsubunits[#parent.directsubunits + 1] = NewUnit
    end

    NewUnit.getAllSubunits = function(self)
        if #self.directsubunits == 0 then return {} end
        local allsubunits = {}

        for _, s in pairs(self.directsubunits) do
            table.insert(allsubunits, s)

            if #s.directsubunits ~= 0 then
                for _, ss in pairs(s.directsubunits) do
                    table.insert(allsubunits, ss)

                    if #ss.directsubunits ~= 0 then
                        for _, sss in pairs(ss.directsubunits) do
                            allsubunits[#allsubunits + 1] = ss
                        end
                    end
                end
            end
        end

        return allsubunits
    end

    NewUnit.hasSubUnit = function(self, subunit)
        for _, subu in pairs(self:getAllSubunits()) do
            if subu == subunit then return true end
        end

        return false
    end

    TrntUnits.Units[#TrntUnits.Units + 1] = NewUnit

    if not NewUnit.parent then
        TrntUnits.TLUnits[#TrntUnits.TLUnits + 1] = NewUnit
    end

    return NewUnit
end

setmetatable(UnitType, {
    __call = UnitType.create
})
