--[[
    Non Rank/Unit/Specialty Config at sh_mainconfig.lua and sv_sql_config.lua
]]

RSYS_GENARMY = RankSystem("GenArmy", 8, {})
RSYS_NAVAL = RankSystem("Naval", 10, {})
RSYS_JEDI = RankSystem("Jedi", 6, {})

for i, t in pairs(RPExtraTeams) do
    if t.category == "Jedi" then
        RSYS_JEDI:addTeam(i)
    elseif t.category == "Navy" then
        RSYS_NAVAL:addTeam(i)
    elseif t.category ~= "Other" and t.category ~= "Staff" then
        RSYS_GENARMY:addTeam(i)
    end
end

--[[
TSR_SetupRank( RankSystem, Index, Name, Prefix, RankClass, Permissions, BonusWeps, Icon, Custom Spawn Function )

Permissions:
Promote = true/false -> Can this rank promote?
MaxPromotion = Number -> Max rank this rank can promote to
Demote = true/false -> Can this rank demote?
MinDemotion = Number -> Max rank this rank can demote to

Leaving MaxPromotion empty will allow to set to highest rank
Leaving MinDemotion empty will allow to set to lowest rank

Icon - Icon path for this ranks icon
]]
--[[ GenArmy Ranks ]]
--Enlisted
RANK_PVT = Rank(RSYS_GENARMY, 1, "Private", "PVT", "Enlisted", false, {"clone_card_c1"}, Material("rankinsignia/trp/PVT.png"))
RANK_PV2 = Rank(RSYS_GENARMY, 2, "Private 2nd Class", "PV2", "Enlisted", false, {"clone_card_c1"}, Material("rankinsignia/trp/PV2.png"))
RANK_PFC = Rank(RSYS_GENARMY, 3, "Private 1st Class", "PFC", "Enlisted", false, {"clone_card_c1"}, Material("rankinsignia/trp/PFC.png"))
RANK_SPC = Rank(RSYS_GENARMY, 4, "Specialist", "SPC", "Enlisted", false, {"clone_card_c1"}, Material("rankinsignia/trp/SPC.png"))
RANK_LCPL = Rank(RSYS_GENARMY, 5, "Lance-Corporal", "LCPL", "Enlisted", false, {"clone_card_c1"}, Material("rankinsignia/trp/LCPL.png"))
RANK_CPL = Rank(RSYS_GENARMY, 6, "Corporal", "CPL", "Enlisted", false, {"clone_card_c1"}, Material("rankinsignia/trp/CPL.png"))

--NCO
RANK_SGT = Rank(RSYS_GENARMY, 7, "Sergeant", "SGT", "NCO", {
    Promote = true,
    Demote = true,
    MaxPromotion = 6,
    MinDemotion = 1
}, {"clone_card_c2","weapon_cuff_police","arrest_stick"}, Material("rankinsignia/trp/SGT.png"))

RANK_SGM = Rank(RSYS_GENARMY, 8, "Sergeant Major", "SGM", "NCO", {
    Promote = true,
    Demote = true,
    MaxPromotion = 6,
    MinDemotion = 1
}, {"clone_card_c2","weapon_cuff_police","arrest_stick"}, Material("rankinsignia/trp/SGM.png"))

--Officer
RANK_2LT = Rank(RSYS_GENARMY, 9, "2nd Lieutenant", "2LT", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 6,
    MinDemotion = 1
}, {"clone_card_c3","weapon_cuff_police","arrest_stick"}, Material("rankinsignia/trp/2LT.png"))

RANK_LT = Rank(RSYS_GENARMY, 10, "Lieutenant", "LT", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 6,
    MinDemotion = 1
}, {"clone_card_c3","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/LT.png"))

RANK_CPT = Rank(RSYS_GENARMY, 11, "Captain", "CPT", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 6,
    MinDemotion = 1
}, {"clone_card_c3","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/CPT.png"))

RANK_MAJ = Rank(RSYS_GENARMY, 12, "Major", "MAJ", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 11,
    MinDemotion = 1
}, {"clone_card_c3","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/MAJ.png"))

RANK_CDR = Rank(RSYS_GENARMY, 13, "Commander", "CDR", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 12,
    MinDemotion = 1
}, {"clone_card_c4","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/CDR.png"))

--[[ Naval Ranks ]]
RANK_CRW = Rank(RSYS_NAVAL, 1, "Crewman", "CRW", "Enlisted", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/PVT.png"))

RANK_PO3 = Rank(RSYS_NAVAL, 2, "Petty Officer 3rd Class", "PO3", "NCO", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/PV2.png"))

RANK_PO2 = Rank(RSYS_NAVAL, 3, "Petty Officer 2nd Class", "PO2", "NCO", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/PFC.png"))

RANK_PO1 = Rank(RSYS_NAVAL, 4, "Petty Officer 1st Class", "PO1", "NCO", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/SPC.png"))

RANK_CPO = Rank(RSYS_NAVAL, 5, "Chief Petty Officer", "CPO", "NCO", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/LCPL.png"))

RANK_SCPO = Rank(RSYS_NAVAL, 6, "Senior Chief Petty Officer", "SCPO", "NCO", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/CPL.png"))

RANK_MCPO = Rank(RSYS_NAVAL, 7, "Master Chief Petty Officer", "MCPO", "NCO", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/1CPL.png"))

RANK_ENS = Rank(RSYS_NAVAL, 10, "Ensign", "Ensign", "Officer", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/SFC.png"))

RANK_NLT = Rank(RSYS_NAVAL, 12, "Lieutenant", "LT", "Officer", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/1SG.png"))

RANK_LCDR = Rank(RSYS_NAVAL, 13, "Lieutenant Commander", "LCDR", "Officer", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/SGM.png"))

RANK_NCDR = Rank(RSYS_NAVAL, 14, "Commander", "CDR", "Officer", {
    Promote = false,
    Demote = false
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/CSM.png"))

RANK_NCPT = Rank(RSYS_NAVAL, 15, "Captain", "CPT", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 13,
    MaxDemotion = 1
}, {"clone_card_c5","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/OC.png"))

RANK_LRADM = Rank(RSYS_NAVAL, 16, "Lower Rear Admiral", "LR Admiral", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 15,
    MaxDemotion = 1
}, {"clone_card_c5","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/2LT.png"))

RANK_URADM = Rank(RSYS_NAVAL, 17, "Upper Rear Admiral", "UR Admiral", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 16,
    MaxDemotion = 1
}, {"clone_card_c5","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/LT.png"))

RANK_VADM = Rank(RSYS_NAVAL, 18, "Vice Admiral", "V Admiral", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 17,
    MaxDemotion = 1
}, {"clone_card_c5","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/CPT.png"))

RANK_ADM = Rank(RSYS_NAVAL, 19, "Admiral", "Admiral", "Officer", {
    Promote = true,
    Demote = true
}, {"clone_card_c5","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/trp/MAJ.png"))

--[[ Jedi Ranks ]]
RANK_JYNG = Rank(RSYS_JEDI, 1, "Jedi Youngling", "JYGN", "Enlisted", {
    Promote = false,
    Demote = false
}, {"clone_card_c1"}, Material("rankinsignia/jedi/initiate.png"))
RANK_JSGT = Rank(RSYS_JEDI, 2, "Jedi Sergeant", "JSGT", "NCO", {
    Promote = false,
    Demote = false
}, {"clone_card_c2"}, Material("rankinsignia/jedi/initiate.png"))

RANK_JCDR = Rank(RSYS_JEDI, 3, "Jedi Commander", "JCDR", "Officer", {
    Promote = false,
    Demote = false
}, {"clone_card_c4","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/jedi/padawan.png"))

RANK_JGN = Rank(RSYS_JEDI, 4, "Jedi General", "JGN", "Officer", {
    Promote = true,
    Demote = false,
    MaxPromotion = 3
}, {"clone_card_c5","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/jedi/knight.png"))

RANK_SJGN = Rank(RSYS_JEDI, 5, "Senior Jedi General", "SJGN", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 4,
    MaxDemotion = 1
}, {"clone_card_c5","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/jedi/master.png"))

RANK_HJGN = Rank(RSYS_JEDI, 6, "High Jedi General", "HJGN", "Officer", {
    Promote = true,
    Demote = true,
    MaxPromotion = 5,
    MaxDemotion = 1
}, {"clone_card_c5","holocomm","weapon_cuff_elastic","arrest_stick"}, Material("rankinsignia/jedi/master.png"))

--[[------------------------------------------------------------------------------]]
--[[ Units 
	UnitType( string Name, unittype Directsubunit, int Quantity )
	
	UT_LEGION:new( unit Parent, string Name, string Prefix, string Leadteam, string Defaultteam, table Teams)
]]
--UnitTypes
UT_Squad = UnitType("Squad", "Player", 5)
UT_Platoon = UnitType("Platoon", UT_Squad, 2)
UT_Company = UnitType("Company", UT_Platoon, 2)
UT_Battalion = UnitType("Battalion", UT_Company, 2)

local jobsof501st = {}
for k, v in pairs(RPExtraTeams) do
    if v.category == "501st Battalion" then
        jobsof501st[#jobsof501st + 1] = k
    end
end

local jobsof212th = {}
for k, v in pairs(RPExtraTeams) do
    if v.category == "212th Battalion" then
        jobsof212th[#jobsof212th + 1] = k
    end
end

--Unassigned - CT
UNIT_UNASSIGNED = UT_Battalion:new(nil, "UnAssigned", "CT", "TEAM_CTCOMMANDER", "TEAM_CT", {TEAM_CTCOMMANDER, TEAM_CT, TEAM_HSECURITY})
--501st
UNIT_501ST = UT_Battalion:new(nil, "501st Battalion", "501st", "TEAM_501REX", "TEAM_501CT", jobsof501st)
UNIT_TORRENTCOMPANY = UT_Company:new(UNIT_501ST, "Torrent Company", "", "TEAM_501FIVES", "TEAM_501TCCT", {
	TEAM_501TCJESSE,
	TEAM_501TCTUP,
	TEAM_501TCKIX,
	TEAM_501TCHARDCASE,
	TEAM_501TCDOGMA,
	TEAM_501TCFIVES,
	TEAM_501TCECHO,
	TEAM_501TCCT
})
UNIT_BACKLASHCOMPANY = UT_Company:new(UNIT_501ST, "Backlash Company", "", "TEAM_501BCLEAD", "TEAM_501CT", {
	TEAM_501BCJETL,
	TEAM_501BCJET,
	TEAM_501MPLEAD,
	TEAM_501VSMEMB,
	TEAM_501BCLEAD,
	TEAM_501BCEM
})
UNIT_MORTUIPLATOON = UT_Platoon:new(UNIT_BACKLASHCOMPANY, "Mortui Platoon", "", "TEAM_501MPLEAD", "", {
	TEAM_501MPLEAD,
	TEAM_501VSMEMB,
	TEAM_501BCEM
})
UNIT_SPECTRESQUAD = UT_Squad:new(UNIT_MORTUIPLATOON, "Spectre Squad", "", "TEAM_501MPLEAD", "TEAM_501VSMEMB", {TEAM_501VSMEMB})
UNIT_EXCURSORSQUAD = UT_Squad:new(UNIT_MORTUIPLATOON, "Excursor Squad", "", "TEAM_501BCLEAD", "TEAM_501BCEM", {TEAM_501BCEM})
--212th
UNIT_212TH = UT_Battalion:new(nil, "212th Battalion", "212th", "TEAM_212CODY", "TEAM_212CT", jobsof212th)
UNIT_GHOSTCOMPANY = UT_Company:new(UNIT_212TH, "Ghost Company", "", "", "", {})
UNIT_2AIRBORNECOMPANY = UT_Company:new(UNIT_212TH, "2nd Airborne Company", "", "", "", {})
--[[------------------------------------------------------------------------------]]
--[[ Specialties
	Specialty( string Name, string Prefix, int UniqueID, table Models, table Weapons, table Teams)
]]
--[[ Clone Trooper Specialties ]]
SPC_LIGHT = Specialty("Light Infantry", "L", 1, nil, {"rw_sw_dc15s"}, RSYS_GENARMY.teams)
SPC_HEAVY = Specialty("Heavy Infantry", "H", 2, nil, {"rw_sw_z6"}, RSYS_GENARMY.teams)
SPC_HEAVY:SetBonusStats( 225, nil, nil, 50 )
SPC_PILOT = Specialty("Pilot", "P", 3, {
    UNIT_501ST = "models/suno_herm/501st/pilot/pilot.mdlmodels/suno_herm/501st/pilot/pilot.mdl",
    UNIT_212TH = "models/player/suno/212th/pilot/pilot.mdl"
}, {"alydus_fusioncutter", "the_flare_gun_update"}, RSYS_GENARMY.teams)

SPC_MED = Specialty("Medic", "M", 4, nil, {"bn_defib", "weapon_sh_healgrenade", "weapon_jew_stimkit"}, RSYS_GENARMY.teams)

SPC_ARF = Specialty("ARF", "ARF", 5, nil, {"rw_sw_valken38x", "realistic_hook"}, RSYS_GENARMY.teams)

SPC_ARC = Specialty("ARC", "ARC", 6, nil, {"rw_sw_westarm5", "realistic_hook"}, RSYS_GENARMY.teams)
SPC_ARC:SetBonusStats( 300, nil, nil, 25 )
SPC_SECURITY = Specialty("Security", "MP", 7, nil, {"weapon_policebaton", "weapon_cuff_elastic", "weapon_policeshield", "clone_card_c3"}, RSYS_GENARMY.teams)

--[[ Jedi Specialties ]]
SPC_ACE = Specialty("Ace", "A", 8, nil, {"alydus_fusioncutter", "the_flare_gun_update"}, RSYS_JEDI.teams)
SPC_RESRCHER = Specialty("Researcher", "R", "9", nil, {}, RSYS_JEDI.teams)
SPC_HEALER = Specialty("Healer", "H", "10", nil, {"weapon_forceheal"}, RSYS_JEDI.team)
SPC_BATTLETRAINER = Specialty("Battle Trainer", "BT", 11, nil, {}, RSYS_JEDI.teams)
SPC_TEACHER = Specialty("Teacher", "T", 12, nil, {}, RSYS_JEDI.teams)
SPC_SBRARTISAN = Specialty("Saber Artisan", "SA", 13, nil, {}, RSYS_JEDI.teams)
SPC_TECHEX = Specialty("Tech Expert", "", 14, nil, {"alydus_fusioncutter"}, RSYS_JEDI.teams)
SPC_PEACEKEEPERS = Specialty("Peace Keepers", "", 15, nil, {}, RSYS_JEDI.teams)
SPC_TMPLGUARD = Specialty("Temple Guard", "", 16, nil, {}, RSYS_JEDI.teams)

--[[ WHATEVER YOU DO, DO NOT TOUCH ANYTHING BELOW THIS LINE ]]

print("[Torrent's Units]: Ranks Config Loaded")

if SERVER then
    hook.Call("TrntUnitsloadedMainSHConfig", nil)
end
