-- Should the rank Icon be shown above the player?
TrntUnits.Config.ShowIconAbovePlayer = true
-- DarkRP Commands for Promotion, Demotion, and SetRank. Will automatically put a /, do not put it here.
TrntUnits.Config.PromotionChatCommand = "promote"
TrntUnits.Config.DemotionChatCommand = "demote"
TrntUnits.Config.SetRankChatCommand = "setrank"
TrntUnits.Config.RemovePlyChatCommand = "removeply"
TrntUnits.Config.SetSPCChatCommand = "setspecialty"
TrntUnits.Config.RemoveSPCChatCommand = "removespecialty"

TrntUnits.Config.MaxPerm = function(ply)
    if ply:IsSuperAdmin() or ply:IsStaff() then return true end

    return false
end