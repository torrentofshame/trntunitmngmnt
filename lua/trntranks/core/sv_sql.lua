--TrntUnits.Config.usemysqloo = true

if TrntUnits.Config.usemysqloo then
    require("mysqloo")

--    TrntUnits.TSRDB = mysqloo.connect("10.42.88.2", "ig_cwrp_roles", "uiPiL725vyHZTJlp", "ig_cwrp_roles", 3306, "/var/run/mysqld/mysqld.sock")

    TrntUnits.TSRDB = mysqloo.connect(unpack(TrntUnits.Config.mysqloo))

    function TrntUnits.TSRDB:onConnected()
        print("[Torrent's Units]: Database has connected!")
        local CreateMainTbl = self:query([[CREATE TABLE IF NOT EXISTS `trntrankstest` (
								`steamid` varchar(255) DEFAULT NULL,
								`ranktable` varchar(255) DEFAULT NULL,
								`spctable` varchar(255) DEFAULT NULL
							) ENGINE=MyISAM DEFAULT CHARSET=utf8]])
        CreateMainTbl:start()
    end

    function TrntUnits.TSRDB:onConnectionFailed(err)
        print("[Torrent's Units]: FAILED TO CONNECT TO MYSQL")
    end

    TrntUnits.TSRDB:connect()

    --Converts SQLite Data to MySQL
    concommand.Add("trntrankstest_convertsql", function(Player, CMD, Args)
        if not Player:JBR_AdminAccess() then return end
        if not Args or not Args[1] then return end
        local Nick = string.lower(Args[1])
        local ply = false

        for k, v in pairs(player.GetAll()) do
            if (string.find(string.lower(v:Nick()), Nick)) then
                ply = v
                break
            end
        end

        if ply then
            local CTbl = sql.Query("SELECT * FROM trntrankstest WHERE steamid = " .. sql.SQLStr(ply:SteamID()) .. "")

            if CTbl then
                local ranktable = CTbl[1].ranktable
                local spctable = CTbl[1].spctable
                ply:DecompileRankData(ranktable)
                ply:DecompileSpcData(spctable)
                local SaveQ = TrntUnits.TSRDB:query("UPDATE trntrankstest SET ranktable=\'" .. ranktable .. "\', spctable=\'" .. spctable .. "\' WHERE steamid='" .. TrntUnits.TSRDB:escape(ply:SteamID()) .. "'")
                SaveQ:start()
            end
        end
    end)

    --Converts SQLite Data to MySQL but overrides current MYSQL Data
    --Restart server IMMEDIATLY after using
    concommand.Add("trntrankstest_convertsql_all", function(Player, CMD, Args)
        if not Player:JBR_AdminAccess() then return end
        local CTbl = sql.Query("SELECT * FROM trntrankstest")

        if CTbl then
            local CleanTbl = TrntUnits.TSRDB:query("DELETE FROM trntrankstest")
            CleanTbl:start()

            for k, v in pairs(CTbl) do
                local InsertPData = TrntUnits.TSRDB:query("INSERT INTO trntrankstest (`steamid`, `ranktable`, `spctable`)VALUES (\'" .. TrntUnits.TSRDB:escape(v.steamid) .. "\', \'" .. v.ranktable .. "\'" .. v.spctable .. "\')")
                InsertPData:start()
            end
        end
    end)
else
    hook.Add("Initialize", "Gotta Get this SQL Table goin", function()
        if not sql.TableExists("trntrankstest") then
            sql.Query("CREATE TABLE trntrankstest (steamid varchar(255), ranktable varchar(255), spctable varchar(255))")
        end
    end)
end

TrntUnits.SQLFuncs = {}

function TrntUnits.SQLFuncs:GetPlyData( ply )
    local steamid = ply:SteamID()
    local pldata
    
    if TrntUnits.Config.usemysqloo then
        local LoadData = TrntUnits.TSRDB:query("SELECT * FROM trntrankstest WHERE steamid = \'" .. TrntUnits.TSRDB:escape(steamid) .. "\';")
        
        function LoadData:onSuccess( data )
            if data and data[1] then
                print("[Torrent's Units] - Found Player")
                ply:DecompileTrntUnitsData(data[1])
                ply:TrntUnits_RefreshData()
            else
                print("[Torrent's Units] - Cannot find player, adding.")
                TrntUnits.SQLFuncs:InsertPlyData(steamid)
                ply:TrntUnits_RefreshData()
            end
        end
        
        LoadData:start()
    else
        local LoadData = sql.Query("SELECT * FROM trntrankstest WHERE steamid = \'" .. sql.SQLStr(steamid, true) .. "\';")

        if LoadData == false then
            print("[Torrent's Ranks] Failed to Retrieve Player Data")
        else
            if LoadData and LoadData[1] then
                print("[Torrent's Units] - Found Player")
                ply:DecompileTrntUnitsData(LoadData[1])
                ply:TrntUnits_RefreshData()
            else
                print("[Torrent's Units] - Cannot find player, adding.")
                TrntUnits.SQLFuncs:InsertPlyData(steamid)
                ply:TrntUnits_RefreshData()
            end
        end
    end
end

function TrntUnits.SQLFuncs:InsertPlyData( steamid )
    if TrntUnits.Config.usemysqloo then
        local InsertData = TrntUnits.TSRDB:query("INSERT INTO trntrankstest (steamid) VALUES (\'" .. TrntUnits.TSRDB:escape(steamid) .. "\');")
        InsertData:start()
    else
        sql.Query("INSERT INTO trntrankstest (steamid) VALUES (\'" .. sql.SQLStr(steamid, true) .. "\');")
    end
end

function TrntUnits.SQLFuncs:UpdatePlyData( steamid, ranktable, spctable )
    if not steamid and ( not ranktable or not spctable ) then return end
    if TrntUnits.Config.usemysqloo then
        if ranktable and ranktable ~= "" and spctable and spctable ~= "" then
            local UpdateData = TrntUnits.TSRDB:query("UPDATE trntrankstest SET ranktable=\'"..TrntUnits.TSRDB:escape(ranktable).."\', spctable=\'"..TrntUnits.TSRDB:escape(spctable).."\' WHERE steamid=\'" .. TrntUnits.TSRDB:escape(steamid) .. "\';")
            function UpdateData:onSuccess() print("[Torrent's Units]: Updated SteamID: "..steamid.." Data Successfully!") end
            UpdateData:start()
        elseif ranktable and ranktable ~= "" then
            local UpdateData = TrntUnits.TSRDB:query("UPDATE trntrankstest SET ranktable=\'"..TrntUnits.TSRDB:escape(ranktable).."\' WHERE steamid=\'" .. TrntUnits.TSRDB:escape(steamid) .. "\';")
            function UpdateData:onSuccess() print("[Torrent's Units]: Updated SteamID: "..steamid.." Rank Data Successfully!") end
            UpdateData:start()
        elseif spctable and spctable ~= "" then
            local UpdateData = TrntUnits.TSRDB:query("UPDATE trntrankstest SET spctable=\'"..TrntUnits.TSRDB:escape(spctable).."\' WHERE steamid=\'" .. TrntUnits.TSRDB:escape(steamid) .. "\';")
            function UpdateData:onSuccess() print("[Torrent's Units]: Updated SteamID: "..steamid.." Specialty Data Successfully!") end
            UpdateData:start()
        end
    elseif not TrntUnits.Config.usemysqloo then
        if ranktable and ranktable ~= "" and spctable and spctable ~= "" then
            local UpdateData = sql.Query("UPDATE trntrankstest SET ranktable=\'"..sql.SQLStr(ranktable, true).."\', spctable=\'"..sql.SQLStr(spctable, true).."\' WHERE steamid=\'" .. sql.SQLStr(steamid, true) .. "\';")
        elseif ranktable and ranktable ~= "" then
            local UpdateData = sql.Query("UPDATE trntrankstest SET ranktable=\'"..sql.SQLStr(ranktable, true).."\' WHERE steamid=\'" .. sql.SQLStr(steamid, true) .. "\';")
        elseif spctable and spctable ~= "" then
            local UpdateData = sql.Query("UPDATE trntrankstest SET spctable=\'"..sql.SQLStr(spctable, true).."\' WHERE steamid=\'" .. sql.SQLStr(steamid, true) .. "\';")
        end
    end
end
