local mousepos

function openrankcontextmenu(ply)
    if not IsValid(ply) then return end
    local menu = DermaMenu()
    menu:AddOption("TrntRanks"):SetIcon("icon16/shield.png")
    menu:AddSpacer()

    --Promote
    menu:AddOption("Promote", function()
        net.Start("trntranks_promotetrp")
        net.WriteString(ply:SteamID())
        net.SendToServer()
    end):SetIcon("icon16/award_star_add.png")

    --Demote
    menu:AddOption("Demote", function()
        net.Start("trntranks_demotetrp")
        net.WriteString(ply:SteamID())
        net.SendToServer()
    end):SetIcon("icon16/award_star_delete.png")

    menu:AddSpacer()
    local RankTbl = ply:GetRankTable()
    
    local setrank
    
    if RankTbl then
        if not setrank then
            setrank = menu:AddSubMenu("SetRank")
        end

        for _, v in pairs(RankTbl.RankSys.ranks) do
            setrank:AddOption(v.prefix, function()
                net.Start("trntranks_settrprank")
                net.WriteString(ply:SteamID())
                net.WriteInt(v.index, 32)
                net.SendToServer()
            end)
        end
    end

    menu:Open()
    input.SetCursorPos(unpack(mousepos))
end

function openspecialtycontextmenu(ply)
    if (not IsValid(ply)) then return end
    local menu = DermaMenu()
    menu:AddOption("TrntSpcs"):SetIcon("icon16/shield.png")
    menu:AddSpacer()
    --SetSpecialty
    local setspecialty

    if LocalPlayer():TrntUnits_HasMaxPerm() then
        if not setspecialty then
            setspecialty = menu:AddSubMenu("SetSpecialty") --:SetIcon("icon16/bullet_wrench.png")
        end

        for _, v in pairs(ply:getPossibleSpecialties()) do
            setspecialty:AddOption(v.name, function()
                net.Start("trntranks_settrpspc")
                net.WriteString(ply:SteamID())
                net.WriteInt(v:GetUID(), 32)
                net.SendToServer()
            end)
        end

        setspecialty:AddOption("Remove Specialty", function()
            net.Start("trntranks_settrpspc")
            net.WriteString(ply:SteamID())
            net.SendToServer()
        end):SetIcon("icon16/bullet_delete.png")
    end

    menu:Open()
    input.SetCursorPos(unpack(mousepos))
end

properties.Add("trntrankmenu", {
    Order = 0,
    MenuLabel = "TrntRanks",
    MenuIcon = "icon16/shield.png",
    Filter = function(self, ent, ply)
        if not IsValid(ent) then return false end
        if not ent:IsPlayer() then return false end
        if ent:IsBot() then return false end
        mousepos = {input.GetCursorPos()}

        return true
    end,
    Action = function(_, ply)
        timer.Simple(0.1, function()
            local menu = DermaMenu()
            menu:AddOption("Loading...", function() end)
            menu:Open()
            input.SetCursorPos(unpack(mousepos))
            openrankcontextmenu(ply)
        end)
    end
})

properties.Add("trntspecialtymenu", {
    Order = 1,
    MenuLabel = "TrntSpcs",
    MenuIcon = "icon16/shield.png",
    Filter = function(self, ent, ply)
        if not IsValid(ent) then return false end
        if not ent:IsPlayer() then return false end
        if ent:IsBot() then return false end
        mousepos = {input.GetCursorPos()}

        return true
    end,
    Action = function(_, ply)
        timer.Simple(0.1, function()
            local menu = DermaMenu()
            menu:AddOption("Loading...", function() end)
            menu:Open()
            input.SetCursorPos(unpack(mousepos))
            openspecialtycontextmenu(ply)
        end)
    end
})
