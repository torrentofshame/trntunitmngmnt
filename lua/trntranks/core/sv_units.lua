util.AddNetworkString("trntranks_promotetrp")
util.AddNetworkString("trntranks_demotetrp")
util.AddNetworkString("trntranks_settrpspc")
util.AddNetworkString("trntranks_settrprank")
local PlayerMeta = FindMetaTable("Player")

TrntUnits.CachedJobVars = TrntUnits.CachedJobVars or {}

function LoadPlayerInit( ply )
    timer.Simple(1, function()
        if IsValid(ply) then TrntUnits.SQLFuncs:GetPlyData( ply ) end
    end)
end

hook.Add("PlayerInitialSpawn", "TrntUnits_LoadPlayerInitiallytoServer", LoadPlayerInit)

--[[ This is to Cache the Team variables so we can save those which will prob never be changed  ]]
hook.Add("loadCustomDarkRPItems", "TrntUnits.CacheDarkRPTeamVars", function()
    timer.Simple(1, function()
        for varname, jobtable in pairs(_G) do
            if string.find(varname, "TEAM_") then
                RunString("function getvarnum() return "..varname.." end")
                if RPExtraTeams[getvarnum()] then
                    TrntUnits.CachedJobVars[getvarnum()] = varname
                end
            end
        end
    end)
end)

--[[ Now for everyone's favorite...Player MetaFunctions!!! ]]
--[[ Handling and Caching Player's data ]]
function PlayerMeta:DecompileTrntUnitsData(CompiledData)
    --Decompile Player Rank
    local TeamandRanks = string.Explode(";", CompiledData.ranktable)

    for _, v in pairs(TeamandRanks) do
        local DecompiledRank = string.Explode(",", v)

        if DecompiledRank and DecompiledRank[1] and DecompiledRank[2] then
            TrntUnits.RankData[self:SteamID()] = TrntUnits.RankData[self:SteamID()] or {}
            RunString("function getCompRank() return "..DecompiledRank[1].." end")
            TrntUnits.RankData[self:SteamID()][getCompRank()] = tonumber(DecompiledRank[2])
        end
    end

    --Decompile Player Specialty
    local SPCTEAMPAIRS = string.Explode(";", CompiledData.spctable)
    for _, v in pairs(SPCTEAMPAIRS) do
        local DecompiledSPC = string.Explode(",", v)

        TrntUnits.SpecialtyData[self:SteamID()] = TrntUnits.SpecialtyData[self:SteamID()] or {}

        if DecompiledSPC and DecompiledSPC[1] and DecompiledSPC[2] then
            RunString("function getCompSPC() return "..DecompiledSPC[1].." end")
            TrntUnits.SpecialtyData[self:SteamID()][getCompSPC()] = tonumber(DecompiledSPC[2])
        end
    end
end

function PlayerMeta:CompileTrntUnitsData()
    local CompiledData = {
        ranktable = "",
        spctable = ""
    }

    --Compile Player Rank
    for k, v in pairs(TrntUnits.RankData[self:SteamID()]) do
        CompiledData.ranktable = CompiledData.ranktable .. TrntUnits.CachedJobVars[k] .. "," .. v .. ";"
    end

    --Compile Player Specialty
    for k, v in pairs(TrntUnits.SpecialtyData[self:SteamID()]) do
        CompiledData.spctable = CompiledData.spctable .. TrntUnits.CachedJobVars[k] .. "," .. v .. ";"
    end

    return CompiledData
end

-------------------------------------------]]
--[[ Refresh and Save Player Data ]]
function PlayerMeta:TrntUnits_RefreshData()
    local Team = self:Team()
    TrntUnits.RankData[self:SteamID()] = TrntUnits.RankData[self:SteamID()] or {}
    TrntUnits.SpecialtyData[self:SteamID()] = TrntUnits.SpecialtyData[self:SteamID()] or {}
    
    local RankSys
    for _, v in pairs(TrntUnits.RankSystems) do
        if table.HasValue(v.teams, Team) then
            RankSys = v
        end
    end

    if not RankSys then return end

    if not TrntUnits.RankData[self:SteamID()] or not TrntUnits.SpecialtyData[self:SteamID()] then
        LoadPlayerInit(self)
    end

    if not TrntUnits.RankData[self:SteamID()][Team] then
        TrntUnits.RankData[self:SteamID()][Team] = 1
        self:TrntUnits_SaveData()
    end

    if not TrntUnits.SpecialtyData[self:SteamID()][Team] then
        TrntUnits.SpecialtyData[self:SteamID()][Team] = 0
        self:TrntUnits_SaveData()
    end

    local rankint = TrntUnits.RankData[self:SteamID()][Team]
    self:SetNWInt("trntrank", rankint)
    local spcint = TrntUnits.SpecialtyData[self:SteamID()][Team]
    self:SetNWInt("trntspc", spcint)

    if self:GetTeamName() then
        self:setDarkRPVar("job", team.GetName(Team) .. " (" .. self:GetTeamName() .. ")")
    end
end

function PlayerMeta:TrntUnits_SaveData()
    local CompiledData = self:CompileTrntUnitsData()
    TrntUnits.SQLFuncs:UpdatePlyData( self:SteamID(), CompiledData.ranktable, CompiledData.spctable )
end

-------------------------------------------]]
function PlayerMeta:GetTeamName()
    local specialty = self:GetSpecialtyTable()
    if specialty then return specialty.name
    else return false end
end

--[[ Changing Someone's Rank ]]
function PlayerMeta:TrntUnits_ChangeRank(Promoter, RankToSet)
    local Team = self:Team()
    local Action = ""

    if RankToSet.index > self:GetRank() then
        Action = "Promoted"
    else
        Action = "Demoted"
    end

    self:SetNWInt("trntrank", RankToSet.index)
    TrntUnits.RankData[self:SteamID()] = TrntUnits.RankData[self:SteamID()] or {}
    TrntUnits.RankData[self:SteamID()][Team] = RankToSet.index
    self:TrntUnits_RefreshData()
    self:TrntUnits_SaveData()
    DarkRP.notify(self, 1, 4, "You have been " .. Action .. " to: " .. RankToSet.name .. ".")

    if Promoter then
        DarkRP.notify(Promoter, 1, 4, "You " .. Action .. " " .. self:Nick() .. " to: " .. RankToSet.name .. ".")
    end
end

--[[ Setting a Player to a Specific Rank ]]
function TrntUnitsSetRankCMD(ply, args)
    if not args or args == "" then return "" end
    local argTbl = string.Explode(" ", args)
    local targetNick = argTbl[1]
    local TargetNewRankPrefix = string.upper(argTbl[2])
    local target = false

    for k, v in pairs(player.GetAll()) do
        if (string.find(string.lower(v:Nick()), string.lower(targetNick))) then
            target = v
            break
        end
    end

    if not target then
        DarkRP.notify(ply, 1, 4, "No player found with this name.")

        return ""
    end

    local targetRankSys

    for _, v in pairs(TrntUnits.RankSystems) do
        if table.HasValue(v.teams, target:Team()) then
            targetRankSys = v
            break
        end
    end

    local TargetNewRank

    for _, r in pairs(targetRankSys.ranks) do
        if string.upper(r.prefix) == string.upper(TargetNewRankPrefix) then
            TargetNewRank = r
            break
        end
    end

    if ply:TrntUnits_HasMaxPerm() then
        if not TargetNewRank then
            DarkRP.notify(ply, 1, 4, "Invalid Rank Entered!")

            return ""
        end

        return target:TrntUnits_ChangeRank(ply, TargetNewRank)
    else
        DarkRP.notify(ply, 1, 4, "You do not have the Permissions Necessary to Perform this Action.")

        return ""
    end
end

net.Receive( "trntranks_settrprank", function( len, ply )
    local targetsteamid = net.ReadString()
    local RankToSetIndex = net.ReadInt(32)
    local target = player.GetBySteamID( targetsteamid )
    if not IsValid(target) or not target:IsPlayer() or not RankToSetIndex then return end
    
    local RankTbl = target:GetRankTable()
    if RankTbl then
        local RankToSet = RankTbl.RankSys.ranks[RankToSetIndex]
        if not RankToSet then
            DarkRP.notify(ply, 1, 4, "Invalid Rank Selected!")
        elseif RankToSet and not ply:TrntUnits_HasMaxPerm() then
            DarkRP.notify(ply, 1, 4, "You do not have the Permissions Necessary to Perform this Action.")
        else
            return target:TrntUnits_ChangeRank(ply, RankToSet)
        end
    end
end)

--[[ Promoting a Player ]]
function TrntUnitsPromotePly(ply, args)
    if not args or args == "" then return "" end
    local target = false

    for k, v in pairs(player.GetAll()) do
        if (string.find(string.lower(v:Nick()), string.lower(args))) then
            target = v
            break
        end
    end

    if not target then
        DarkRP.notify(ply, 1, 4, "No player found with this name.")

        return ""
    end

    local hasperm = false

    if ply:TrntUnits_HasMaxPerm() then
        hasperm = true
    elseif ply:GetRankTable():canPromote(target:GetRankTable()) and ply:GetTLUnit().teams.Lead == ply:Team() and ply:GetTLUnit().team and ply:GetTLUnit() == target:GetTLUnit() then
        hasperm = true
    end

    if hasperm then
        local TargetNewRank = target:GetRankTable():OffsetRank(1)

        if not TargetNewRank then
            DarkRP.notify(ply, 1, 4, "This Player cannot be Promoted further!")

            return ""
        end

        return target:TrntUnits_ChangeRank(ply, TargetNewRank)
    else
        DarkRP.notify(ply, 1, 4, "You do not have the Permissions Necessary to Perform this Action.")

        return ""
    end
end

net.Receive("trntranks_promotetrp", function(len, ply)
    local targetsteamid = net.ReadString()
    local target = player.GetBySteamID(targetsteamid)
    if not target or not target:IsPlayer() then return end
    local hasperm = false

    if ply:TrntUnits_HasMaxPerm() then
        hasperm = true
    elseif ply:GetRankTable():canPromote(target:GetRankTable()) and ply:GetUnit().teams.Lead == ply:GetUnit().team and (ply:GetUnit():hasSubUnit(target:GetUnit()) or ply:GetUnit() == target:GetUnit()) then
        hasperm = true
    end

    if hasperm then
        local TargetNewRank = target:GetRankTable():OffsetRank(1)

        if not TargetNewRank then
            DarkRP.notify(ply, 1, 4, "This Player cannot be Promoted further!")

            return ""
        end

        return target:TrntUnits_ChangeRank(ply, TargetNewRank)
    else
        DarkRP.notify(ply, 1, 4, "You do not have the Permissions Necessary to Perform this Action.")

        return ""
    end
end)

--[[ Demoting a Player ]]
function TrntUnitsDemotePly(ply, args)
    if not args or args == "" then return "" end
    local target = false

    for k, v in pairs(player.GetAll()) do
        if (string.find(string.lower(v:Nick()), string.lower(args[1]))) then
            target = v
            break
        end
    end

    if not target then
        DarkRP.notify(ply, 1, 4, "No player found with this name.")

        return ""
    end

    local hasperm = false

    if ply:TrntUnits_HasMaxPerm() then
        hasperm = true
    elseif ply:GetRankTable():canDemote(target:GetRankTable()) and ply:GetUnit().teams.Lead == ply:GetUnit().team and (ply:GetUnit():hasSubUnit(target:GetUnit()) or ply:GetUnit() == target:GetUnit()) then
        hasperm = true
    end

    if hasperm then
        local TargetNewRank = target:GetRankTable():OffsetRank(-1)

        if not TargetNewRank then
            DarkRP.notify(ply, 1, 4, "This Player cannot be Demoted further!")

            return ""
        end

        return target:TrntUnits_ChangeRank(ply, TargetNewRank)
    else
        DarkRP.notify(ply, 1, 4, "You do not have the Permissions Necessary to Perform this Action.")

        return ""
    end
end

net.Receive("trntranks_demotetrp", function(len, ply)
    local targetsteamid = net.ReadString()
    local target = player.GetBySteamID(targetsteamid)
    if not target or not target:IsPlayer() then return end
    local hasperm = false

    if ply:TrntUnits_HasMaxPerm() then
        hasperm = true
    elseif ply:GetRankTable():canDemote(target:GetRankTable()) and ply:GetUnit().teams.Lead == ply:GetUnit().team and (ply:GetUnit():hasSubUnit(target:GetUnit()) or ply:GetUnit() == target:GetUnit()) then
        hasperm = true
    end

    if hasperm then
        local TargetNewRank = target:GetRankTable():OffsetRank(-1)

        if not TargetNewRank then
            DarkRP.notify(ply, 1, 4, "This Player cannot be Demoted further!")

            return ""
        end

        return target:TrntUnits_ChangeRank(ply, TargetNewRank)
    else
        DarkRP.notify(ply, 1, 4, "You do not have the Permissions Necessary to Perform this Action.")

        return ""
    end
end)

--[[----------------------------------------------------------------------------------------------------------------]]
--[[ Changing Someone's Specialty ]]
function TrntUnitsSetSPC(ply, args)
    if not args or args == "" then return "" end
    if not ply:TrntUnits_HasMaxPerm() then return "" end
    local argTbl = string.Explode(" ", args)
    local targetNick = argTbl[1]
    local SpecialtyToAddName = table.concat(argTbl, "", 2)
    local target = false

    for k, v in pairs(player.GetAll()) do
        if (string.find(string.lower(v:Nick()), string.lower(targetNick))) then
            target = v
            break
        end
    end

    if not target then
        DarkRP.notify(ply, 1, 4, "No player found with this name.")

        return ""
    end

    local availspc = target:getPossibleSpecialties()
    local specialtytoadd

    for _, s in pairs(availspc) do
        if s.name == SpecialtyToAddName then
            specialtytoadd = s
            break
        end
    end

    if not specialtytoadd then
        DarkRP.notify(ply, 1, 4, "Invalid specialty./The target cannot have this specialty.")

        return ""
    end

    TrntUnits.SpecialtyData[target:SteamID()][target:Team()] = specialtytoadd:GetUID()
    target:TrntUnits_RefreshData()
    target:TrntUnits_SaveData()
    DarkRP.notify(ply, 1, 4, "You gave " .. target:Nick() .. " the specialty: " .. SpecialtyToAddName .. ".")
    DarkRP.notify(target, 1, 4, "You received the specialty: " .. SpecialtyToAddName .. ".")

    return ""
end

function TrntUnitsRemoveSPC(ply, args)
    if not args or args == "" then return "" end
    if not ply:TrntUnits_HasMaxPerm() then return "" end
    local targetNick = args
    local target = false

    for k, v in pairs(player.GetAll()) do
        if (string.find(string.lower(v:Nick()), string.lower(targetNick))) then
            target = v
            break
        end
    end

    if not target then
        DarkRP.notify(ply, 1, 4, "No player found with this name.")

        return ""
    end

    TrntUnits.SpecialtyData[target:SteamID()][target:Team()] = 0
    target:setDarkRPVar( "job", team.GetName( target:Team() ) )
    target:TrntUnits_RefreshData()
    target:TrntUnits_SaveData()
    DarkRP.notify(ply, 1, 4, "You removed " .. target:Nick() .. "'s specialty.")
    DarkRP.notify(target, 1, 4, "Your Specialty was removed.")

    return ""
end

net.Receive("trntranks_settrpspc", function( len, ply )
    local SteamID = net.ReadString()
    local target = player.GetBySteamID(SteamID)
    local SPCUID = net.ReadInt(32) or 0
    if not target then return end
    if not ply:TrntUnits_HasMaxPerm() then return end
    TrntUnits.SpecialtyData[target:SteamID()][target:Team()] = SPCUID
    target:TrntUnits_RefreshData()
    target:TrntUnits_SaveData()

    if SPCUID == 0 then
        target:setDarkRPVar( "job", team.GetName( target:Team() ) )
        DarkRP.notify(ply, 1, 4, "You removed " .. target:Nick() .. "'s specialty.")
        DarkRP.notify(target, 1, 4, "Your Specialty was removed.")
    else
        local SpecialtyToAddName = TrntUnits.Specialties[SPCUID].name
        DarkRP.notify(ply, 1, 4, "You gave " .. target:Nick() .. " the specialty: " .. SpecialtyToAddName .. ".")
        DarkRP.notify(target, 1, 4, "You received the specialty: " .. SpecialtyToAddName .. ".")
    end
end)

--[[-----------------------------------------------------------------------------]]
--[[ Hook Time! ]]
hook.Add("OnPlayerChangedTeam", "TrntUnits.OnPlayerChangedTeam", function(ply, old, new)
    ply:TrntUnits_RefreshData()
end)

hook.Add("PlayerLoadout", "TrntUnits.PlayerLoadout", function(ply)
    local RankTbl = ply:GetRankTable()
    local SpecialtyTbl = ply:GetSpecialtyTable()

    if RankTbl then
        local RankWeps = RankTbl:GetWeps()

        if RankWeps and #RankWeps > 0 then
            for _, wep in pairs(RankWeps) do
                ply:Give(wep)
            end
        end
    end

    if SpecialtyTbl then
        local SpecialtyWeps = SpecialtyTbl:GetWeps()

        if SpecialtyWeps and #SpecialtyWeps > 0 then
            for _, wep in pairs(SpecialtyWeps) do
                ply:Give(wep)
            end
        end
    end
end)

hook.Add("playerGetSalary", "TrntUnits.playerGetSalary", function(ply, baseamnt)
    local RankTbl = ply:GetRankTable()

    if RankTbl then
        local AddedBonus = RankTbl:findBonusSalaryChange(baseamnt)
        local CalcSalary = baseamnt + AddedBonus

        return false, "Payday! You received $" .. baseamnt .. " + $" .. AddedBonus .. " for your current rank!", CalcSalary
    end
end)

hook.Add("PlayerSpawn", "TrntUnits.PlayerSpawn", function(ply)
    local SpecialtyTable = ply:GetSpecialtyTable()

    if SpecialtyTable and SpecialtyTable.models then
        for unit, mdlpath in pairs(SpecialtyTable.models) do
            if table.HasValue(ply:getUnits(), unit) then
                timer.Simple(0.5, function()
                    ply:SetModel(mdlpath)
                end)
                --ply:SendLua("RPExtraTeams["..ply:Team().."].model[#RPExtraTeams["..ply:Team().."].model+1] = "\""..mdlpath.."\"")
            end
        end
    end
    
    timer.Simple( 5, function() --Gotta do this after DarkRPG Does its stuff
        if SpecialtyTable and SpecialtyTable.bonusstats ~= {} then
            if SpecialtyTable.bonusstats.health and SpecialtyTable.bonusstats.health ~= 0 then
                ply:SetMaxHealth( SpecialtyTable.bonusstats.health )
                ply:SetHealth( SpecialtyTable.bonusstats.health )
            end
            if SpecialtyTable.bonusstats.walkspeed and SpecialtyTable.bonusstats.walkspeed ~= 0 then
                local newwalkspeed = ply:GetWalkSpeed() + SpecialtyTable.bonusstats.walkspeed
                ply:SetWalkSpeed( newwalkspeed )
            end
            if SpecialtyTable.bonusstats.runspeed and SpecialtyTable.bonusstats.runspeed ~= 0 then
                local newrunspeed = ply:GetRunSpeed() + SpecialtyTable.bonusstats.runspeed
                ply:SetRunSpeed( newrunspeed )
            end
            if SpecialtyTable.bonusstats.armor and SpecialtyTable.bonusstats.armor ~= 0 then
                ply:SetArmor( SpecialtyTable.bonusstats.armor )
            end
        end
        print("[Torrent's Units] - Set Player's Specialty Bonuses!")
    end)
end)
