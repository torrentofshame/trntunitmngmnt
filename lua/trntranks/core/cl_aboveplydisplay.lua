if TrntUnits.Config.ShowIconAbovePlayer then
    local PlayerMeta = FindMetaTable("Player")

    local colors = {
        black = Color(0, 0, 0, 255),
        blue = Color(0, 0, 255, 255),
        brightred = Color(200, 30, 30, 255),
        darkred = Color(0, 0, 70, 100),
        darkblack = Color(0, 0, 0, 200),
        gray1 = Color(0, 0, 0, 155),
        gray2 = Color(51, 58, 51, 100),
        red = Color(255, 0, 0, 255),
        white = Color(255, 255, 255, 255),
        white1 = Color(255, 255, 255, 200)
    }

    local Page = Material("icon16/page_white_text.png")

    PlayerMeta.drawPlayerInfo = function(self)
        local pos = self:EyePos()
        pos.z = pos.z + 10
        pos = pos:ToScreen()

        if not self:getDarkRPVar("wanted") then
            pos.y = pos.y - 50
        end

        local Team = self:Team()

        if GAMEMODE.Config.showname then
            local nick = self:Nick()
            draw.DrawNonParsedText(nick, "DarkRPHUD2", pos.x + 1, pos.y + 1, colors.black, 1)
            draw.DrawNonParsedText(nick, "DarkRPHUD2", pos.x, pos.y, RPExtraTeams[Team] and RPExtraTeams[Team].color or team.GetColor(Team), 1)
            local IconMaterial = self:GetRankIcon()

            if IconMaterial then
                surface.SetFont("DarkRPHUD2")
                local TW, TH = surface.GetTextSize(nick)
                surface.SetMaterial(IconMaterial)
                surface.SetDrawColor(255, 255, 255, 255)
                surface.DrawTexturedRect(pos.x - TW / 2 - 17, pos.y, 16, 16)
            end
        end

        if GAMEMODE.Config.showhealth then
            local health = DarkRP.getPhrase("health", self:Health())
            draw.DrawNonParsedText(health, "DarkRPHUD2", pos.x + 1, pos.y + 21, colors.black, 1)
            draw.DrawNonParsedText(health, "DarkRPHUD2", pos.x, pos.y + 20, colors.white1, 1)
        end

        if GAMEMODE.Config.showjob then
            local teamname = self:getDarkRPVar("job") or team.GetName(self:Team())
            draw.DrawNonParsedText(teamname, "DarkRPHUD2", pos.x + 1, pos.y + 41, colors.black, 1)
            draw.DrawNonParsedText(teamname, "DarkRPHUD2", pos.x, pos.y + 40, colors.white1, 1)
        end

        if self:getDarkRPVar("HasGunlicense") then
            surface.SetMaterial(Page)
            surface.SetDrawColor(255, 255, 255, 255)
            surface.DrawTexturedRect(pos.x - 16, pos.y + 60, 32, 32)
        end
    end
end
