local PlayerMeta = FindMetaTable("Player")

function PlayerMeta:TrntUnits_HasMaxPerm()
    return TrntUnits.Config.MaxPerm(self)
end

--[[ Player Rank Info ]]
function PlayerMeta:GetRank()
    return self:GetNWInt("trntrank", 1)
end

function PlayerMeta:GetRankTable()
    local rankindex = self:GetRank()
    if not rankindex then return false end

    for _, v in pairs(TrntUnits.RankSystems) do
        if table.HasValue(v.teams, self:Team()) then return v.ranks[rankindex] end
    end
    return false
end

function PlayerMeta:GetRankName()
    local RankTbl = self:GetRankTable()
    if RankTbl then
        return RankTbl.name
    end
end

function PlayerMeta:GetRankPrefix()
    local RankTbl = self:GetRankTable()
    if RankTbl then
        return RankTbl.prefix
    end
end

function PlayerMeta:GetRankClass()
    local RankTbl = self:GetRankTable()
    if RankTbl then
        return RankTbl.rankclass
    end
end

function PlayerMeta:GetRankIcon()
    local RankTbl = self:GetRankTable()
    if RankTbl then
        return RankTbl.icon
    end
end

--[[ Player Unit Info ]]
function PlayerMeta:getUnits()
    local units = {}

    for _, unit in pairs(TrntUnits.Units) do
        if table.HasValue(unit.teams, self:Team()) then
            units[unit.unittype] = unit
        end
    end

    return units
end

function PlayerMeta:GetTLUnit()
    for _, v in pairs(self:getUnits()) do
        if not v.parent then
            return v
        end
    end
end

--[[ Player Specialty Info ]]
--List all the specialties a player can have
function PlayerMeta:getPossibleSpecialties()
    local availablespecialties = {}

    for uid, spc in pairs(TrntUnits.Specialties) do
        if table.HasValue(spc.teams, self:Team()) then
            availablespecialties[uid] = spc
        end
    end

    return availablespecialties
end

function PlayerMeta:GetSpecialty()
    return self:GetNWInt("trntspc", 0)
end

function PlayerMeta:GetSpecialtyTable()
    local spcuid = self:GetSpecialty()
    if not spcuid or spcuid == 0 then return false end

    return TrntUnits.Specialties[spcuid]
end

function PlayerMeta:GetRankUnitName()
    local units = self:getUnits()
    if units then
        for _,v in pairs(units) do
            if not v.parent then
                return v.name
            end
        end
    end
end

--[[ DarkRP Shit ]]
hook.Add("loadCustomDarkRPItems", "TrntUnits_LoadingDarkRPShit", function()
    --[[ Defining Commands ]]
    --[[ Rank Shit ]]
    -- Promotion
    if SERVER then
        DarkRP.defineChatCommand(TrntUnits.Config.PromotionChatCommand, TrntUnitsPromotePly)
    end

    DarkRP.declareChatCommand{
        command = TrntUnits.Config.PromotionChatCommand,
        description = "Promote Player",
        delay = 1
    }

    --Demotion
    if SERVER then
        DarkRP.defineChatCommand(TrntUnits.Config.DemotionChatCommand, TrntUnitsDemotePly)
    end

    DarkRP.declareChatCommand{
        command = TrntUnits.Config.DemotionChatCommand,
        description = "Demote Player",
        delay = 1
    }

    --SetRank
    if SERVER then
        DarkRP.defineChatCommand(TrntUnits.Config.SetRankChatCommand, TrntUnitsSetRankCMD)
    end

    DarkRP.declareChatCommand{
        command = TrntUnits.Config.SetRankChatCommand,
        description = "Set Player's Rank",
        delay = 1
    }

    --[[-----------------------------------------------
		 Specialty Shit ]]
    --Add Spc
    if SERVER then
        DarkRP.defineChatCommand(TrntUnits.Config.SetSPCChatCommand, TrntUnitsSetSPC)
    end

    DarkRP.declareChatCommand{
        command = TrntUnits.Config.SetSPCChatCommand,
        description = "Add SPC to Player.",
        delay = 1
    }

    --Remove Spc
    if SERVER then
        DarkRP.defineChatCommand(TrntUnits.Config.RemoveSPCChatCommand, TrntUnitsRemoveSPC)
    end

    DarkRP.declareChatCommand{
        command = TrntUnits.Config.RemoveSPCChatCommand,
        description = "Remove SPC from Player.",
        delay = 1
    }

    --[[------------------------------------------------------------------
	     Override Player Name metamethod ]]
    PlayerMeta.SteamName = PlayerMeta.SteamName or PlayerMeta.Name

    function PlayerMeta:Name()
        if not self:IsValid() then
            DarkRP.error("Attempt to call Name/Nick/GetName on a non-existing player!", SERVER and 1 or 2)
        end

        local RankTbl = self:GetRankTable()
        local SpecialtyTbl = self:GetSpecialtyTable()
        local Unit = {}

        if RankTbl then
            for _, v in pairs(self:getUnits()) do
                if not v.parent then
                    Unit = v
                    break
                end
            end

            local Nick = GAMEMODE.Config.allowrpnames and self:getDarkRPVar("rpname") or self:SteamName()

            if Unit and Unit.prefix and Unit.prefix ~= "" and SpecialtyTbl and SpecialtyTbl.prefix and SpecialtyTbl.prefix ~= "" then
                Nick = Unit.prefix .. " " .. SpecialtyTbl.prefix .. " " .. RankTbl.prefix .. " " .. Nick
            elseif SpecialtyTbl and SpecialtyTbl.prefix and SpecialtyTbl.prefix ~= "" then
                Nick = SpecialtyTbl.prefix .. " " .. RankTbl.prefix .. " " .. Nick
            elseif Unit and Unit.prefix and Unit.prefix ~= "" then
                Nick = Unit.prefix .. " " .. RankTbl.prefix .. " " .. Nick
            else
                Nick = RankTbl.prefix .. " " .. Nick
            end

            return Nick
        else
            local Nick = GAMEMODE.Config.allowrpnames and self:getDarkRPVar("rpname") or self:SteamName()

            return Nick
        end
    end

    PlayerMeta.GetName = PlayerMeta.Name
    PlayerMeta.Nick = PlayerMeta.Name
end)
