print("--------------------------------")
print("||      Loading Torrent's Units      ||")
print("||         Created by: /tɔɹəntʊfʃeɪm/        ||")
print("||      Licensed to: Unlicensed        ||")
print("||      ©2019 - TorrentofShame.com     ||")
print("--------------------------------")

TrntUnits = TrntUnits or {}
TrntUnits.Config = TrntUnits.Config or {}
if SERVER then
	TrntUnits.RankData = TrntUnits.RankData or {}
	TrntUnits.SpecialtyData = TrntUnits.SpecialtyData or {}
end

local files, dirs = file.Find("trntranks/*", "LUA")

print("| [Torrent's Units] - Loading Scripts |")

if SERVER then
    include("trntranks/sv_trnt_sqlconfig.lua")
    print("| trntranks/sv_trnt_sqlconfig.lua |")
end

include("trntranks/sh_mainconfig.lua")
AddCSLuaFile("trntranks/sh_mainconfig.lua")
print("| trntranks/sh_mainconfig.lua |")

for _, d in pairs(dirs) do --Stuff in ./core and ./metaobjs
    local dfs, _ = file.Find("trntranks/" .. d .. "/*.lua", "LUA")

    for _, f in pairs(dfs) do
        if string.sub(f, 1, 3) == "sv_" then
            if SERVER then
                include("trntranks/" .. d .. "/" .. f)
                print("| trntranks/" .. d .. "/" .. f .. " |")
            end
        elseif string.sub(f, 1, 3) == "cl_" then
            AddCSLuaFile("trntranks/" .. d .. "/" .. f)
            if CLIENT then
                include("trntranks/" .. d .. "/" .. f)
            end
            print("| trntranks/" .. d .. "/" .. f .. " |")
        elseif string.sub(f, 1, 3) == "sh_" then
            AddCSLuaFile("trntranks/" .. d .. "/" .. f)
            include("trntranks/" .. d .. "/" .. f)
            print("| trntranks/" .. d .. "/" .. f .. " |")
        end
    end
end

hook.Add("loadCustomDarkRPItems", "TrntUnits.InitSHConfig", function()
    timer.Simple( 3, function()
        
        for _, f in pairs(files) do --Config Files
            if string.sub(f, 1, 3) ~= "sv_" and f ~= "sh_mainconfig.lua" then
                include("trntranks/" .. f)
                AddCSLuaFile("trntranks/" .. f)
                print("| trntranks/" .. f .. " |")
            end
        end
        print("| [Torrent's Units] - Finished Loading Scripts |")
        
    end)
end)
